<?php

require_once __DIR__ . '/../vendor/autoload.php';

$badgeOptions = \FlyingAnvil\BadgeGenerator\Application\DataObject\BadgeOptions::create('left', 'right');
$badgeOptions->setLeftText('direction');
$badgeOptions->setRadius(3);
$badgeOptions->setLeftBackgroundColor(\FlyingAnvil\BadgeGenerator\Application\DataObject\Color::createFromHex('00cc00'));

$badgeGenerator = new \FlyingAnvil\BadgeGenerator\Application\Service\Generator();
$badge          = $badgeGenerator->generate($badgeOptions);

echo $badge;
