# Badge Generator
Tool for generating badges, like

![coverage][coverage]
![php-version][php-version]

[coverage]: https://gitlab.com/flying-anvil/badge-generator/raw/master/gitlab-data/badges/coverage.svg "Coverage"
[php-version]: https://gitlab.com/flying-anvil/badge-generator/raw/master/gitlab-data/badges/php-version.svg "php-version"

# How To

## Executable Script

Run the `generate.php` script to generate a badge.

It accespts some parameters to customize the result:

|short|long                  |description                                             |default |
|-----|:---------------------|--------------------------------------------------------|--------|
|o    |output                |file to write to                                        |stdout  |
|l    |left                  |specifies the left text                                 |        |
|r    |right                 |specifies the right text                                |        |
|g    |no-gradient           |no value, removes the gradient in the badge             |omitted |
|p    |no-percentage-coloring|no value, disables auto-coloring based on the right text|omitted |
|     |radius                |radius of the edges                                     |`5.0`   |
|     |left-text-color       |color of the left text                                  |`eeeeee`|
|     |right-text-color      |color of the right text                                 |`eeeeee`|
|     |left-background-color |background color of the left segment                    |`555555`|
|     |right-background-color|background color of the right segment                   |`4B3D6C`|
|     |low-percent-color     |right background color for 0% when using auto coloring  |`B31329`|
|     |mid-percent-color     |right background color for 50% when using auto coloring |`CDAB58`|
|     |hight-percent-color   |right background color for 100% when using auto coloring|`2AC258`|

## Using Generator

Create a Generator and feed it with some options:
```php
<?php

require_once __DIR__ . '/../vendor/autoload.php';

$badgeOptions = \FlyingAnvil\BadgeGenerator\Application\DataObject\BadgeOptions::create('left', 'right');
$badgeOptions->setLeftText('direction');
$badgeOptions->setRadius(3);
$badgeOptions->setLeftBackgroundColor(\FlyingAnvil\BadgeGenerator\Application\DataObject\Color::createFromHex('00cc00'));

$badgeGenerator = new \FlyingAnvil\BadgeGenerator\Application\Service\Generator();
$badge          = $badgeGenerator->generate($badgeOptions);

echo $badge;
```
