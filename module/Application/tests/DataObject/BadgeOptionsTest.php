<?php

declare(strict_types=1);

namespace FlyingAnvil\BadgeGenerator\Application\Test\DataObject;

use FlyingAnvil\BadgeGenerator\Application\DataObject\BadgeOptions;
use FlyingAnvil\BadgeGenerator\Application\DataObject\Color;
use PHPUnit\Framework\TestCase;

/**
 * @covers \FlyingAnvil\BadgeGenerator\Application\DataObject\BadgeOptions
 */
class BadgeOptionsTest extends TestCase
{
    /** @var BadgeOptions */
    private $options;

    protected function setUp(): void
    {
        $this->options = BadgeOptions::create('left', 'right');
    }

    public function testDefaultValues()
    {
        self::assertEquals('eeeeee' , $this->options->getLeftTextColor()->getHexRepresentation());
        self::assertEquals('eeeeee' , $this->options->getRightTextColor()->getHexRepresentation());

        self::assertEquals('555555', $this->options->getLeftBackgroundColor()->getHexRepresentation());
        self::assertEquals('4b3d6c', $this->options->getRightBackgroundColor()->getHexRepresentation());

        self::assertEquals('b31329', $this->options->getLowPercentColor()->getHexRepresentation());
        self::assertEquals('cdab58', $this->options->getMidPercentColor()->getHexRepresentation());
        self::assertEquals('2ac258', $this->options->getHighPercentColor()->getHexRepresentation());

        self::assertFalse($this->options->usePercentageColoring());

        $otherOptions = BadgeOptions::create('coverage', '100%');
        self::assertTrue($otherOptions->usePercentageColoring());
        self::assertEquals(100, $otherOptions->getPercentage());
    }

    public function testCanSetAndGetLeftText()
    {
        self::assertEquals('left', $this->options->getLeftText());
        
        $this->options->setLeftText('other text');
        self::assertEquals('other text', $this->options->getLeftText());
    }

    public function testCanSetAndGetRightText()
    {
        self::assertEquals('right', $this->options->getRightText());
        
        $this->options->setRightText('other text');
        self::assertEquals('other text', $this->options->getRightText());
    }

    public function testCanSetAndGetLeftTextColor()
    {
        $color = Color::createFromChannels(123, 12, 1);
        $this->options->setLeftTextColor($color);
        
        self::assertSame($color, $this->options->getLeftTextColor());
    }

    public function testCanSetAndGetRightTextColor()
    {
        $color = Color::createFromChannels(123, 12, 1);
        $this->options->setRightTextColor($color);
        
        self::assertSame($color, $this->options->getRightTextColor());
    }

    public function testCanSetAndGetLeftBackgroundColor()
    {
        $color = Color::createFromChannels(123, 12, 1);
        $this->options->setLeftBackgroundColor($color);
        
        self::assertSame($color, $this->options->getLeftBackgroundColor());
    }

    public function testCanSetAndGetRightBackgroundColor()
    {
        $color = Color::createFromChannels(123, 12, 1);
        $this->options->setRightBackgroundColor($color);
        
        self::assertSame($color, $this->options->getRightBackgroundColor());
    }

    public function testCanSetAndGetRadius()
    {
        $radius = 9001;

        $this->options->setRadius($radius);
        self::assertEquals($radius, $this->options->getRadius());
    }

    public function testCanSetAndGetUseGradient()
    {
        $useIt = false;
        $this->options->setUseGradient($useIt);

        self::assertEquals($useIt, $this->options->useGradient());
    }

    public function testCanSetAndGetLowPercentageColor()
    {
        $color = Color::createFromChannels(123, 12, 1);
        $this->options->setLowPercentColor($color);
        
        self::assertSame($color, $this->options->getLowPercentColor());
    }

    public function testCanSetAndGetMidPercentageColor()
    {
        $color = Color::createFromChannels(123, 12, 1);
        $this->options->setMidPercentColor($color);
        
        self::assertSame($color, $this->options->getMidPercentColor());
    }

    public function testCanSetAndGetHighPercentageColor()
    {
        $color = Color::createFromChannels(123, 12, 1);
        $this->options->setHighPercentColor($color);
        
        self::assertSame($color, $this->options->getHighPercentColor());
    }

    public function testCanSetAndGetUsePercentageColoring()
    {
        $useIt = false;
        $this->options->setUsePercentageColoring($useIt);

        self::assertEquals($useIt, $this->options->usePercentageColoring());
    }
}
