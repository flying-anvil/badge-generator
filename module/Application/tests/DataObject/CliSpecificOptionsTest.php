<?php

declare(strict_types=1);

namespace FlyingAnvil\BadgeGenerator\Application\Test\DataObject;

use FlyingAnvil\BadgeGenerator\Application\DataObject\BadgeOptions;
use FlyingAnvil\BadgeGenerator\Application\DataObject\CliSpecificOptions;
use PHPUnit\Framework\TestCase;

/**
 * @covers \FlyingAnvil\BadgeGenerator\Application\DataObject\CliSpecificOptions
 */
class CliSpecificOptionsTest extends TestCase
{
    public function testCanSetAndGetBadgeOptions()
    {
        $badgeOptions = BadgeOptions::create('left', 'right');
        $cliOptions   = CliSpecificOptions::create();
        $cliOptions->setBadgeOptions($badgeOptions);

        $result = $cliOptions->getBadgeOptions();
        self::assertSame($badgeOptions, $result);
        self::assertEquals('left', $result->getLeftText());
        self::assertEquals('right', $result->getRightText());
    }

    public function testCanSetAndGetOutputFile()
    {
        $cliOptions = CliSpecificOptions::create();
        $cliOptions->setOutputFile(__DIR__ . '/out.svg');

        self::assertEquals(__DIR__ . '/out.svg', $cliOptions->getOutputFile());
    }
}
