<?php

declare(strict_types=1);

namespace FlyingAnvil\BadgeGenerator\Application\Test\DataObject;

use FlyingAnvil\BadgeGenerator\Application\DataObject\Color;
use FlyingAnvil\BadgeGenerator\Application\Exception\InvalidHexPatternException;
use PHPUnit\Framework\TestCase;

/**
 * @covers \FlyingAnvil\BadgeGenerator\Application\DataObject\Color
 */
class ColorTest extends TestCase
{
    public function testCanGetR()
    {
        $color = Color::createFromHex('abcdef');

        self::assertEquals(171, $color->getRed());
        self::assertEquals('ab', $color->getHexRed());
    }

    public function testCanGetGreen()
    {
        $color = Color::createFromHexChannels('ab', 'cd', 'ef');

        self::assertEquals(205, $color->getGreen());
        self::assertEquals('cd', $color->getHexGreen());
    }

    public function testCanGetBlue()
    {
        $color = Color::createFromHex('abcdef');

        self::assertEquals(239, $color->getBlue());
        self::assertEquals('ef', $color->getHexBlue());
    }

    public function testCanGetColorAsString()
    {
        $color    = Color::createFromChannels(130, 125, 40);
        $expected = '827d28';

        self::assertEquals($expected, (string)$color);
        self::assertEquals($expected, $color->getHexRepresentation());
    }

    public function testCanCreateWithVeryShortHex()
    {
        $color = Color::createFromHex('d');
        self::assertEquals('dddddd', $color->getHexRepresentation());
    }

    public function testCanCreateWithShortHex()
    {
        $color = Color::createFromHex('def');
        self::assertEquals('ddeeff', $color->getHexRepresentation());
    }

    public function testBrokenHexPatternThrowsException()
    {
        $this->expectException(InvalidHexPatternException::class);
        $this->expectExceptionMessage('"trolol" is not hexadecimal');

        Color::createFromHex('trolol');
    }

    public function testInvalidHexPatternThrowsException()
    {
        $this->expectException(InvalidHexPatternException::class);
        $this->expectExceptionMessage('"12354312" is no valid hex representation of a color');

        Color::createFromHex('12354312');
    }
}
