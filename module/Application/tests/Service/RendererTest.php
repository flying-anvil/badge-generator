<?php

declare(strict_types=1);

namespace FlyingAnvil\BadgeGenerator\Application\Test\Service;

use FlyingAnvil\BadgeGenerator\Application\Exception\FileNotFoundException;
use FlyingAnvil\BadgeGenerator\Application\Service\Renderer;
use PHPUnit\Framework\TestCase;

/**
 * @covers \FlyingAnvil\BadgeGenerator\Application\Service\Renderer
 */
class RendererTest extends TestCase
{
    /** @var Renderer */
    private $renderer;

    protected function setUp(): void
    {
        $this->renderer = new Renderer();
    }

    public function testCanRenderTemplate()
    {
        $result = $this->renderer->render(__DIR__ . '/_files/template.phtml', ['text' => 'content']);
        self::assertEquals('<p>content</p>' . "\n", $result);
    }

    public function testCannotRenderUnexistingTemplate()
    {
        $this->expectException(FileNotFoundException::class);
        $this->expectExceptionMessage('no_file not found!');

        $this->renderer->render('no_file');
    }
}
