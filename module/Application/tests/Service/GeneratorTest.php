<?php

declare(strict_types=1);

namespace FlyingAnvil\BadgeGenerator\Application\Test\Service;

use FlyingAnvil\BadgeGenerator\Application\DataObject\BadgeOptions;
use FlyingAnvil\BadgeGenerator\Application\Service\Generator;
use PHPUnit\Framework\TestCase;

/**
 * @covers \FlyingAnvil\BadgeGenerator\Application\Service\Generator
 */
class GeneratorTest extends TestCase
{
    /** @var Generator */
    private $generator;

    protected function setUp(): void
    {
        $this->generator = new Generator();
    }

    public function testCanGenerateBadge()
    {
        $options  = BadgeOptions::create('left', 'right');
        $result   = explode("\n", $this->generator->generate($options));
        $expected = explode("\n", file_get_contents(__DIR__ . '/_files/result.xml'));

        foreach ($result as $lineNumber => $line) {
            self::assertEquals(trim($expected[$lineNumber]), trim($line));
        }
    }
}
