<?php

declare(strict_types=1);

namespace FlyingAnvil\BadgeGenerator\Application\Test\Exception;

use FlyingAnvil\BadgeGenerator\Application\Exception\InvalidHexPatternException;
use PHPUnit\Framework\TestCase;

/**
 * @covers \FlyingAnvil\BadgeGenerator\Application\Exception\InvalidHexPatternException
 */
class InvalidHexPatternExceptionTest extends TestCase
{
    public function testCanGetRepresentation()
    {
        $representation = 'owieth';
        $exception      = new InvalidHexPatternException($representation, 'no message');

        self::assertEquals($representation, $exception->getRepresentation());
    }
}
