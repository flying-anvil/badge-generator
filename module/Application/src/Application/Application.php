<?php

declare(strict_types=1);

namespace FlyingAnvil\BadgeGenerator\Application\Application;

use FlyingAnvil\BadgeGenerator\Application\DataObject\BadgeOptions;
use FlyingAnvil\BadgeGenerator\Application\DataObject\CliSpecificOptions;
use FlyingAnvil\BadgeGenerator\Application\DataObject\Color;
use FlyingAnvil\BadgeGenerator\Application\Exception\DirectoryNotFoundException;
use FlyingAnvil\BadgeGenerator\Application\Exception\ExitExeption;
use FlyingAnvil\BadgeGenerator\Application\Exception\FileNotFoundException;
use FlyingAnvil\BadgeGenerator\Application\Exception\InvalidHexPatternException;
use FlyingAnvil\BadgeGenerator\Application\Service\Generator;

class Application
{
    /** @var Generator */
    private $generator;

    public function __construct()
    {
        $this->generator = new Generator();
    }

    /**
     * @return int
     * @throws ExitExeption
     * @throws DirectoryNotFoundException
     */
    public function run(): int
    {
        try {
            $options    = $this->getCliParameter();
            $badge      = $this->generator->generate($options->getBadgeOptions());
            $outputFile = $options->getOutputFile();

            if ($outputFile === '') {
                echo $badge;
                return 0;
            }

            $outputFileDirectory = dirname($outputFile);
            if (!is_dir($outputFileDirectory)) {
                throw new DirectoryNotFoundException(sprintf(
                    'cannot save badge to "%s". directory "%s" does not exist',
                    $outputFile,
                    $outputFileDirectory
                ));
            }

            file_put_contents($outputFile, $badge);

            return 0;
        } catch (FileNotFoundException $exception) {
            throw new ExitExeption($exception->getMessage());
        }
    }

    /**
     * @return CliSpecificOptions
     * @throws ExitExeption
     */
    private function getCliParameter(): CliSpecificOptions
    {
        // TODO: display help when unknown parameter is given
        // unknown parameter also causes following parameters to vanish

        $rawOpts = getopt('o:l:r:g::p::', [
            'output:',
            'left:',
            'right:',
            'radius:',
            'no-gradient::',
            'no-percentage-coloring::',
            # region Color Stuff
            'left-text-color:',
            'right-text-color:',
            'left-background-color:',
            'right-background-color:',
            'low-percent-color:',
            'mid-percent-color:',
            'high-percent-color:',
            # endregion
        ]);

        $cliOptions = CliSpecificOptions::create();
        $cliOptions->setOutputFile($rawOpts['output'] ?? $rawOpts['o'] ?? '');

        $badgeOptions = BadgeOptions::create(
            $rawOpts['left'] ?? $rawOpts['l'] ?? '',
            $rawOpts['right'] ?? $rawOpts['r'] ?? ''
        );
        $cliOptions->setBadgeOptions($badgeOptions);

        if (isset($rawOpts['radius'])) {
            $badgeOptions->setRadius((float)$rawOpts['radius']);
        }

        if (isset($rawOpts['no-gradient']) || isset($rawOpts['g'])) {
            $badgeOptions->setUseGradient(false);
        }

        if (isset($rawOpts['no-percentage-coloring']) || isset($rawOpts['p'])) {
            $badgeOptions->setUsePercentageColoring(false);
        }

        # region Color Stuff
        try {
            if (isset($rawOpts['left-text-color'])) {
                $badgeOptions->setLeftTextColor(Color::createFromHex($rawOpts['left-text-color']));
            }

            if (isset($rawOpts['right-text-color'])) {
                $badgeOptions->setRightTextColor(Color::createFromHex($rawOpts['right-text-color']));
            }

            if (isset($rawOpts['left-background-color'])) {
                $badgeOptions->setLeftBackgroundColor(Color::createFromHex($rawOpts['left-background-color']));
            }

            if (isset($rawOpts['right-background-color'])) {
                $badgeOptions->setRightBackgroundColor(Color::createFromHex($rawOpts['right-background-color']));
            }

            if (isset($rawOpts['low-percent-color'])) {
                $badgeOptions->setLowPercentColor(Color::createFromHex($rawOpts['low-percent-color']));
            }

            if (isset($rawOpts['mid-percent-color'])) {
                $badgeOptions->setMidPercentColor(Color::createFromHex($rawOpts['mid-percent-color']));
            }

            if (isset($rawOpts['high-percent-color'])) {
                $badgeOptions->setHighPercentColor(Color::createFromHex($rawOpts['high-percent-color']));
            }

        } catch (InvalidHexPatternException $exception) {
            throw new ExitExeption(sprintf(
                'cannot create color "%s", %s',
                $exception->getRepresentation(),
                $exception->getMessage()
            ));
        }
        # endregion

        return $cliOptions;
    }
}
