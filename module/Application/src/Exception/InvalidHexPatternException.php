<?php

declare(strict_types=1);

namespace FlyingAnvil\BadgeGenerator\Application\Exception;

use Exception;
use Throwable;

class InvalidHexPatternException extends Exception
{
    private $representation;

    public function __construct(string $representation, $message = '', $code = 0, Throwable $previous = null)
    {
        $this->representation = $representation;

        parent::__construct($message, $code, $previous);
    }

    public function getRepresentation(): string
    {
        return $this->representation;
    }
}
