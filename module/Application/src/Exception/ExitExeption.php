<?php

declare(strict_types=1);

namespace FlyingAnvil\BadgeGenerator\Application\Exception;

use Exception;

class ExitExeption extends Exception
{
}
