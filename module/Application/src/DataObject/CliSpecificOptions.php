<?php

declare(strict_types=1);

namespace FlyingAnvil\BadgeGenerator\Application\DataObject;

final class CliSpecificOptions
{
    /** @var BadgeOptions */
    private $badgeOptions;

    /** @var string */
    private $outputFile = '';

    private function __construct()
    {}

    public static function create(): CliSpecificOptions
    {
        return new self();
    }

    # region getter and setter
    public function getBadgeOptions(): BadgeOptions
    {
        return $this->badgeOptions;
    }

    public function setBadgeOptions(BadgeOptions $badgeOptions): CliSpecificOptions
    {
        $this->badgeOptions = $badgeOptions;
        return $this;
    }

    public function getOutputFile(): string
    {
        return $this->outputFile;
    }

    public function setOutputFile(string $outputFile): CliSpecificOptions
    {
        $this->outputFile = $outputFile;
        return $this;
    }
    # endregion
}
