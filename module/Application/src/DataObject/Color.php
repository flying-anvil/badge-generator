<?php

declare(strict_types=1);

namespace FlyingAnvil\BadgeGenerator\Application\DataObject;

use FlyingAnvil\BadgeGenerator\Application\Exception\InvalidHexPatternException;

final class Color
{
    /** @var int */
    private $red;

    /** @var int */
    private $green;

    /** @var int */
    private $blue;

    private function __construct(int $red, int $green, int $blue)
    {
        $this->red   = $red;
        $this->green = $green;
        $this->blue  = $blue;
    }

    /**
     * @param string $hex
     * @return Color
     * @throws InvalidHexPatternException
     */
    public static function createFromHex(string $hex): self
    {
        if (!ctype_xdigit($hex)) {
            throw new InvalidHexPatternException($hex, sprintf(
                '"%s" is not hexadecimal',
                $hex
            ));
        }

        $strlen = strlen($hex);

        switch ($strlen) {
            case 1:
                $red   = $hex . $hex;
                $green = $hex . $hex;
                $blue  = $hex . $hex;

                break;
            case 3:
                $parts = str_split($hex);
                $red   = str_repeat($parts[0], 2);
                $green = str_repeat($parts[1], 2);
                $blue  = str_repeat($parts[2], 2);

                break;
            case 6:
                $parts = str_split($hex, 2);
                list($red, $green, $blue) = $parts;

                break;
            default:
                throw new InvalidHexPatternException($hex, sprintf(
                    '"%s" is no valid hex representation of a color',
                    $hex
                ));
        }

        $red   = hexdec($red);
        $green = hexdec($green);
        $blue  = hexdec($blue);

        return new self($red, $green, $blue);
    }

    public static function createFromHexChannels(string $red, string $green, string $blue): self
    {
        return new self(
            hexdec($red),
            hexdec($green),
            hexdec($blue)
        );
    }

    public static function createFromChannels(int $red, int $green, int $blue): self
    {
        return new self($red, $green, $blue);
    }

    # region getter and setter
    public function getHexRepresentation(): string
    {
        return $this->getHexRed() . $this->getHexGreen() . $this->getHexBlue();
    }

    public function __toString()
    {
        return $this->getHexRepresentation();
    }

    public function getRed(): int
    {
        return $this->red;
    }

    public function getGreen(): int
    {
        return $this->green;
    }

    public function getBlue(): int
    {
        return $this->blue;
    }

    public function getHexRed(): string
    {
        return str_pad(dechex($this->red), 2, '0', STR_PAD_LEFT);
    }

    public function getHexGreen(): string
    {
        return str_pad(dechex($this->green), 2, '0', STR_PAD_LEFT);
    }

    public function getHexBlue(): string
    {
        return str_pad(dechex($this->blue), 2, '0', STR_PAD_LEFT);
    }
    # endregion
}
