<?php

declare(strict_types=1);

namespace FlyingAnvil\BadgeGenerator\Application\DataObject;

final class BadgeOptions
{
    /** @var string */
    private $leftText;

    /** @var string */
    private $rightText;

    /** @var Color */
    private $leftTextColor;

    /** @var Color */
    private $rightTextColor;

    /** @var Color */
    private $leftBackgroundColor;

    /** @var Color */
    private $rightBackgroundColor;

    /** @var Color */
    private $lowPercentColor;

    /** @var Color */
    private $midPercentColor;

    /** @var Color */
    private $highPercentColor;

    /** @var float */
    private $radius = 5.0;

    /** @var float */
    private $percentage = 0.0;

    /** @var bool */
    private $useGradient = true;

    /** @var bool */
    private $usePercentageColoring = false;

    private function __construct(string $leftText, string $rightText) {
        $this->leftText  = $leftText;
        $this->rightText = $rightText;

        $this->leftTextColor        = Color::createFromHex('e');
        $this->rightTextColor       = Color::createFromHex('e');
        $this->leftBackgroundColor  = Color::createFromHex('5');
        $this->rightBackgroundColor = Color::createFromHex('4B3D6C');

        $this->lowPercentColor  = Color::createFromHex('B31329'); // wine red
        $this->midPercentColor  = Color::createFromHex('CDAB58'); // topaz  dark yellow
        $this->highPercentColor = Color::createFromHex('2AC258'); // emerald green

        if ($this->calculatePercentage($rightText)) {
            $this->usePercentageColoring = true;
        }
    }

    public static function create(string $leftText, string $rightText): self
    {
        return new self($leftText, $rightText);
    }

    private function calculatePercentage(string $input): bool
    {
        if (preg_match('/^(\d+([.,]\d+)?)\D*%$/', $input, $matches)) {
            $percentage       = min($matches[1], 100.0);
            $this->percentage = (float)str_replace(',', '.', $percentage);

            return true;
        }

        return false;
    }

    # region getter setter
    public function getLeftText(): string
    {
        return $this->leftText;
    }

    public function setLeftText(string $leftText): BadgeOptions
    {
        $this->leftText = $leftText;
        return $this;
    }

    public function getRightText(): string
    {
        return $this->rightText;
    }

    public function setRightText(string $rightText): BadgeOptions
    {
        $this->calculatePercentage($rightText);

        $this->rightText = $rightText;
        return $this;
    }

    public function getLeftTextColor(): Color
    {
        return $this->leftTextColor;
    }

    public function setLeftTextColor(Color $leftTextColor): BadgeOptions
    {
        $this->leftTextColor = $leftTextColor;
        return $this;
    }

    public function getRightTextColor(): Color
    {
        return $this->rightTextColor;
    }

    public function setRightTextColor(Color $rightTextColor): BadgeOptions
    {
        $this->rightTextColor = $rightTextColor;
        return $this;
    }

    public function getLeftBackgroundColor(): Color
    {
        return $this->leftBackgroundColor;
    }

    public function setLeftBackgroundColor(Color $leftBackgroundColor): BadgeOptions
    {
        $this->leftBackgroundColor = $leftBackgroundColor;
        return $this;
    }

    public function getRightBackgroundColor(): Color
    {
        return $this->rightBackgroundColor;
    }

    public function setRightBackgroundColor(Color $rightBackgroundColor): BadgeOptions
    {
        $this->rightBackgroundColor = $rightBackgroundColor;
        return $this;
    }

    public function setRadius(float $radius): BadgeOptions
    {
        $this->radius = $radius;
        return $this;
    }

    public function getRadius(): float
    {
        return $this->radius;
    }

    public function useGradient(): bool
    {
        return $this->useGradient;
    }

    public function setUseGradient(bool $useGradient): BadgeOptions
    {
        $this->useGradient = $useGradient;
        return $this;
    }

    public function getLowPercentColor(): Color
    {
        return $this->lowPercentColor;
    }

    public function setLowPercentColor(Color $lowPercentColor): BadgeOptions
    {
        $this->lowPercentColor = $lowPercentColor;
        return $this;
    }

    public function getHighPercentColor(): Color
    {
        return $this->highPercentColor;
    }

    public function setHighPercentColor(Color $highPercentColor): BadgeOptions
    {
        $this->highPercentColor = $highPercentColor;
        return $this;
    }

    public function usePercentageColoring(): bool
    {
        return $this->usePercentageColoring;
    }

    public function setUsePercentageColoring(bool $usePercentageColoring): BadgeOptions
    {
        $this->usePercentageColoring = $usePercentageColoring;
        return $this;
    }

    public function getPercentage(): float
    {
        return $this->percentage;
    }

    public function getMidPercentColor(): Color
    {
        return $this->midPercentColor;
    }

    public function setMidPercentColor(Color $midPercentColor): BadgeOptions
    {
        $this->midPercentColor = $midPercentColor;
        return $this;
    }
    # endregion
}
