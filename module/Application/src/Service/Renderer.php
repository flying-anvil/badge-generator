<?php

declare(strict_types=1);

namespace FlyingAnvil\BadgeGenerator\Application\Service;

use FlyingAnvil\BadgeGenerator\Application\Exception\FileNotFoundException;

class Renderer
{
    /**
     * @param string $_path
     * @param array $_params
     * @return string
     * @throws FileNotFoundException
     */
    public function render(string $_path, array $_params = []): string
    {
        if (!file_exists($_path)) {
            throw new FileNotFoundException($_path . ' not found!');
        }

        foreach ($_params as $_key => $_value) {
            $$_key = $_value;
        }

        unset($_key, $_value);

        ob_start();

        require $_path;

        $rendered = ob_get_clean();

        return $rendered ?? '';
    }
}
