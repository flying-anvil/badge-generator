<?php

declare(strict_types=1);

namespace FlyingAnvil\BadgeGenerator\Application\Service;

use FlyingAnvil\BadgeGenerator\Application\DataObject\BadgeOptions;
use FlyingAnvil\BadgeGenerator\Application\DataObject\Color;
use FlyingAnvil\BadgeGenerator\Application\Exception\FileNotFoundException;

class Generator
{
    /** @var Renderer */
    private $renderer;

    public function __construct()
    {
        $this->renderer = new Renderer();
    }

    /**
     * @param BadgeOptions $badgeOptions
     * @return string
     * @throws FileNotFoundException
     */
    public function generate(BadgeOptions $badgeOptions): string
    {
        $params = [
            'badgeOptions'     => $badgeOptions,
            'interpolateColor' => [$this, 'interpolateColor'],
        ];

        return $this->renderer->render(__DIR__ . '/../../template/badge.phtml', $params);
    }

    public function interpolateColor(Color $low, Color $mid, Color $high, float $percentage): Color
    {
        if ($percentage > 49.9 && $percentage < 50.9) {
            return $mid;
        }

        if ($percentage > 50.5) {
            return $this->lerpColor($mid, $high, $this->map(0.5, 1.0, 0.0, 1.0, $percentage * 0.01));
        }

        return $this->lerpColor($low, $mid, $percentage * 0.01 * 2);
    }

    private function lerpColor(Color $a, Color $b, float $time): Color
    {
        $newR = $this->lerp($a->getRed(), $b->getRed(), $time);
        $newG = $this->lerp($a->getGreen(), $b->getGreen(), $time);
        $newB = $this->lerp($a->getBlue(), $b->getBlue(), $time);

        return Color::createFromChannels($newR, $newG, $newB);
    }

    private function lerp(int $a, int $b, float $time): int
    {
        return (int)((($b - $a) * $time) + $a);
    }

    private function map(float $inFrom, float $inTo, float $outFrom, float $outTo, float $value): float
    {
        return $outFrom + ((($value - $inFrom) * ($outTo - $outFrom)) / ($inTo - $inFrom));
    }
}
